// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the STCOMPUTING_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// STCOMPUTING_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef STCOMPUTING_EXPORTS
#define STCOMPUTING_API __declspec(dllexport)
#else
#define STCOMPUTING_API __declspec(dllimport)
#endif

// This class is exported from the STComputing.dll
class STCOMPUTING_API CSTComputing {
private:
	double _ta_k;
	double _ta_n;
	double _deltaT;
	double _deltaF;
	double _ta;
	double _mf;
	double _fn;
	std::vector<std::vector<double>> traces;
	std::vector<double> Tgrup(std::vector<double> mas, double n, double m);
	std::vector<double> ComputeTrace(std::vector<double> mass);

public:
	CSTComputing::CSTComputing(std::vector<std::vector<double>> traces, double deltaF, double mf, double fn);

	std::vector<std::vector<double>> Compute();

	void SetTaK(double tak);
	double GetTaK();

	void SetTaN(double tan);
	double GetTaN();

	void SetDeltaT(double deltaT);
	double GetDeltaT();

	void SetTa(double ta);
	double GetTa();

	void SetMf(double mf);
	double GetMf();
};

extern STCOMPUTING_API int nSTComputing;

STCOMPUTING_API int fnSTComputing(void);
