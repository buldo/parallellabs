// STComputing.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "STComputing.h"
#include "math.h"


// This is an example of an exported variable
STCOMPUTING_API int nSTComputing=0;

// This is an example of an exported function.
STCOMPUTING_API int fnSTComputing(void)
{
	return 42;
}

CSTComputing::CSTComputing(std::vector<std::vector<double>> traces, double deltaF, double mf, double fn)
{
	this->traces = traces;
	_deltaF = deltaF;
	_mf = mf;
	_fn = fn;
}

std::vector<std::vector<double>> CSTComputing::Compute()
{
	auto tracesResult = new std::vector<std::vector<double>>();
	for (auto trace : traces)
	{
		tracesResult->push_back(ComputeTrace(trace));
	}
	return *tracesResult;
}

std::vector<double> CSTComputing::Tgrup(std::vector<double> mas, double n, double m)
{
	int Nc = floor(n / 2);
	std::vector<double> ph2;
	for (int k = 0; k < m;k++)
	{
		double Re = 0;
		double Im = 0;
		double Re1 = 0;
		double Im1 = 0;
		for (int i = 0; i < n;i++)
		{
			Re += mas[i] * cos(2 * M_PI * (i - Nc) * _deltaT * (_fn + k * _deltaF));
			Im += mas[i] * sin(2 * M_PI * (i - Nc) * _deltaT * (_fn + k * _deltaF));
			Re1 -= mas[i] * 2 * M_PI * (i - Nc) * _deltaT * sin(2 * M_PI * (i - Nc) * _deltaT * (_fn + k * _deltaF));
			Im1 += mas[i] * 2 * M_PI * (i - Nc) * _deltaT * cos(2 * M_PI * (i - Nc) * _deltaT * (_fn + k * _deltaF));
		}
		ph2.push_back(-(Im1*Re - Re1 * Im) / (Re*Re + Im * Im));
	}
	return ph2;
}

std::vector<double> CSTComputing::ComputeTrace(std::vector<double> mass)
{
	double i_ta_k = _ta_k / _deltaT;
	double i_ta_n = _ta_n / _deltaT;
	double NsdV = i_ta_k - i_ta_n + 1;

	double NinTa = floor(_ta / _deltaT) + 1;
	double NinTa2 = (NinTa - 1) / 2;

	std::vector<double> fk;

	for (int k = 0; k < NsdV - 1;k++)
	{
		std::vector<double> tmp;
		for (int j = 0; j < NinTa; j++)
		{
			tmp.push_back(mass[floor(i_ta_n - NinTa2 + k + j)]);
		}
		auto phase = Tgrup(tmp, NinTa, _mf);
		double sum = 0;
		for (int j = 0; j < _mf; j++)
		{
			//if (n_Wf == 0)
			//{
				sum += cos((_fn + j * _deltaF) * phase[j]);
			//}
		}
		fk.push_back(sum);
	}

	return fk;
}

void CSTComputing::SetTaK(double tak)
{
	_ta_k = tak;
}

double CSTComputing::GetTaK()
{
	return _ta_k;
}

void CSTComputing::SetTaN(double tan)
{
	_ta_n = tan;
}

double CSTComputing::GetTaN()
{
	return _ta_n;
}

void CSTComputing::SetDeltaT(double deltaT)
{
	this->_deltaT = deltaT;
}

double CSTComputing::GetDeltaT()
{
	return _deltaT;
}

void CSTComputing::SetTa(double ta)
{
	this->_ta = ta;
}

double CSTComputing::GetTa()
{
	return _ta;
}

void CSTComputing::SetMf(double mf)
{
	this->_mf = mf;
}

double CSTComputing::GetMf()
{
	return _mf;
}
