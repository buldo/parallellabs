// ����������� ���� ���� ifdef - ��� ����������� ����� �������� ��������, ���������� ��������� 
// �������� �� ��������� DLL. ��� ����� ������ DLL �������������� � �������������� ������� MTCOMPUTING_EXPORTS,
// � ��������� ������. ���� ������ �� ������ ���� ��������� � �����-���� �������
// ������������ ������ DLL. ��������� ����� ����� ������ ������, ��� �������� ����� �������� ������ ����, ����� 
// ������� MTCOMPUTING_API ��� ��������������� �� DLL, ����� ��� ������ DLL ����� �������,
// ������������ ������ ��������, ��� ����������������.
#ifdef MTCOMPUTING_EXPORTS
#define MTCOMPUTING_API __declspec(dllexport)
#else
#define MTCOMPUTING_API __declspec(dllimport)
#endif

// ���� ����� ������������� �� MTComputing.dll
class MTCOMPUTING_API CMTComputing {
public:
	CMTComputing(void);
private:
	double _ta_k;
	double _ta_n;
	double _deltaT;
	double _deltaF;
	double _ta;
	double _mf;
	double _fn;
	std::vector<std::vector<double>> traces;
	std::vector<double> Tgrup(std::vector<double> mas, double n, double m);
	std::vector<double> ComputeTrace(std::vector<double> mass);

public:
	CMTComputing::CMTComputing(std::vector<std::vector<double>> traces, double deltaF, double mf, double fn);

	std::vector<std::vector<double>> Compute();

	void SetTaK(double tak);
	double GetTaK();

	void SetTaN(double tan);
	double GetTaN();

	void SetDeltaT(double deltaT);
	double GetDeltaT();

	void SetTa(double ta);
	double GetTa();

	void SetMf(double mf);
	double GetMf();
};

extern MTCOMPUTING_API int nMTComputing;

MTCOMPUTING_API int fnMTComputing(void);
