// Interface.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <memory>

#define REPEAT_CNT 5

int _tmain(int argc, _TCHAR* argv[])
{
	double dF = 1.0;
	double mF = 51;
	double fN = 10;
	std::ifstream file;
	file.open("1082.txt");
	int tracesCnt = 1082;
	if (file.bad())
	{
		return -1;
	}

	std::vector<double> inputValues;

	std::string inStr;
	while (std::getline(file, inStr))
	{
		inputValues.push_back(std::stod(inStr));
	}

	std::vector<std::vector<double>> inputMatrix;

	auto traceLenght = inputValues.size()/tracesCnt;

	for (int i = 0; i < tracesCnt;i++)
	{
		std::vector<double> trace;
		for (int j = 1; j < traceLenght - 1; j++)
		{
			trace.push_back(inputValues[i*j]);
		}
		inputMatrix.push_back(trace);
	}

	std::shared_ptr<CSTComputing> singleThreadComputer(new CSTComputing(inputMatrix, dF, mF, fN));
	singleThreadComputer->SetDeltaT(0.002);
	singleThreadComputer->SetMf(51);
	singleThreadComputer->SetTa(0.06);
	singleThreadComputer->SetTaK(0.164);
	singleThreadComputer->SetTaN(0.1);

	long long singleDuration = 0;
	for (int i = 0; i < REPEAT_CNT; i++)
	{
		auto singleStart = std::chrono::high_resolution_clock::now();
		auto a = singleThreadComputer->Compute();
		auto singleStop = std::chrono::high_resolution_clock::now();
		singleDuration += std::chrono::duration_cast<std::chrono::milliseconds>(singleStop - singleStart).count();
	}
	singleDuration = singleDuration / REPEAT_CNT;

	std::shared_ptr<CMTComputing> multyThreadComputer(new CMTComputing(inputMatrix, dF, mF, fN));
	multyThreadComputer->SetDeltaT(0.002);
	multyThreadComputer->SetMf(51);
	multyThreadComputer->SetTa(0.06);
	multyThreadComputer->SetTaK(0.164);
	multyThreadComputer->SetTaN(0.1);

	long long multyDuration = 0;
	for (int i = 0; i < REPEAT_CNT; i++)
	{
		auto multyStart = std::chrono::high_resolution_clock::now();
		auto b = multyThreadComputer->Compute();
		auto multyStop = std::chrono::high_resolution_clock::now();
		multyDuration += std::chrono::duration_cast<std::chrono::milliseconds>(multyStop - multyStart).count();
	}
	multyDuration = multyDuration / REPEAT_CNT;

	std::cout << "Single: " << singleDuration << "\n";
	std::cout << "Multy: " << multyDuration << "\n";

	getchar();
	
	return 0;
}

